//
//  MDoctor.swift
//  TestMap
//
//  Created by Van Nguyen on 2/24/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import CoreData

class MDoctor: NSObject {
    var name: String
    var picture: String
    var age:Int
    var city: String
    
    override init() {
        self.name = ""
        self.picture = ""
        self.age = 0
        self.city = ""
    }
    
    // MARK: Init with params
    init(name: String, picture: String, age: Int, city: String) {
        self.name = name
        self.picture = picture
        self.age = age
        self.city = city
    }
    
    // MARK: Init with Dict
    init(dict: NSDictionary) {
        if let tempName = dict.value(forKey: Constants.kFullNameKey) as? String {
            self.name = tempName
        } else {
            self.name = ""
        }
        
        if let tempPicture = dict.value(forKey: Constants.kPictureKey) as? String {
            self.picture = tempPicture
        } else {
            self.picture = ""
        }
        
        if let tempAge = dict.value(forKey: Constants.kAgeKey) as? String {
            self.age = Int(tempAge)!
        } else {
            self.age = 0
        }
        
        if let tempCity = dict.value(forKey: Constants.kCityKey) as? String {
            self.city = tempCity
        } else {
            self.city = ""
        }
    }
    
    // MARK: Init with NSManagedObject
    init(object: NSManagedObject) {
        self.name = object.value(forKey: Constants.kNameKey) as! String
        self.picture = object.value(forKey: Constants.kPictureKey) as! String
        self.age = object.value(forKey: Constants.kAgeKey) as! Int
        self.city = object.value(forKey: Constants.kCityKey) as! String
    }
}
