//
//  ViewController.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import ReachabilitySwift
import MapKit

class RootViewController: UIViewController {

    // Local Variable
    var reachability: Reachability = Reachability()!
    var mapVC: MapViewController?
    var doctorsVC: DoctorsViewController?
    var networkStatus: NetworkStatus = NetworkStatus.NetworkStatusNone
    var currentView: TopView = .RootView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        // Stop Reachability
        reachability.stopNotifier()
        
        // Remove Observer
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Setup View
    func setupView() {
        // Init Reachability
        reachability = Reachability()!
        
        // Add Observer
        NotificationCenter.default.addObserver( self,
                                                selector: #selector(self.reachabilityChanged),
                                                name: ReachabilityChangedNotification,
                                                object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
        
        // Init MapViewController
        let sb = UIStoryboard.init(name: StoryboardName.Main, bundle: nil)
        mapVC = sb.instantiateViewController(withIdentifier: ViewControllerName.MapViewController) as? MapViewController
        
        // Init TableView
        doctorsVC = sb.instantiateViewController(withIdentifier: ViewControllerName.DoctorsViewController) as? DoctorsViewController
        
        // Init data
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                // Reachable via WiFi
                
                // Post data
                APIManager.getRegion(deviceInfo: "Test iPhone", userInfo: "Van Nguyen", completion: { (lat, long, radius) in
                    self.mapVC?.monitoringRegion = CLCircularRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), radius: radius, identifier: "MonitoringRegion")
                    
                    // Show Map VC
                    self.currentView = .MapView
                    Utils.addSubviewIntoView(insertedView: (self.mapVC?.view)!, parentView: self.view)
                })
            } else {
                // Reachable via Cellular
                
                // Post data
                APIManager.getDoctors(latitude: 21.017045, longitude: 105.829707) { (doctors) in
                    // Show Tableview VC
                    self.currentView = .ListView
                    Utils.addSubviewIntoView(insertedView: (self.doctorsVC?.view)!, parentView: self.view)
                }
        
            }
        } else {
            // Network not reachable
            self.currentView = .RootView
            Utils.showAlertInView(title: "No Network Connection", message: nil, vc: self)
        }

    }
}

// MARK: Reachability Handler
extension RootViewController {
    func reachabilityChanged(notification: NSNotification) {
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
                networkStatus = .NetworkStatusWiFi
            } else {
                print("Reachable via Cellular")
                networkStatus = .NetworkStatusCellular
            }
        } else {
            print("Network not reachable")
            Utils.showAlertInView(title: "No Network Connection", message: nil, vc: self)
            networkStatus = .NetworkStatusNone
        }
        
        self.updateTopView()
    }
    
    func getCurrentNetworkStatus() -> NetworkStatus {
        return networkStatus
    }
    
    func updateCurrentNetworkStatus() {
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                networkStatus = .NetworkStatusWiFi
            } else {
                networkStatus = .NetworkStatusCellular
            }
        } else {
            networkStatus = .NetworkStatusNone
        }
    }
    
    func updateTopView() {
        if (networkStatus == .NetworkStatusNone) {
            if (currentView == .MapView) {
                // Animation to dismiss view
                self.mapVC?.view.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: { 
                    self.mapVC?.view.alpha = 0.0
                }, completion: { (finished) in
                    self.mapVC?.view.removeFromSuperview()
                })
            } else if (currentView == .ListView) {
                // Animation to dismiss view
                self.doctorsVC?.view.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: {
                    self.doctorsVC?.view.alpha = 0.0
                }, completion: { (finished) in
                    self.doctorsVC?.view.removeFromSuperview()
                })
            }
            self.currentView = .RootView
        } else if ((networkStatus == .NetworkStatusWiFi) && (currentView != .MapView)) {
            if (currentView == .ListView) {
                // Animation to dismiss view
                self.doctorsVC?.view.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: {
                    self.doctorsVC?.view.alpha = 0.0
                }, completion: { (finished) in
                    self.doctorsVC?.view.removeFromSuperview()
                })
            }
            // Get new info and push view
            APIManager.getRegion(deviceInfo: "Test iPhone", userInfo: "Van Nguyen", completion: { (lat, long, radius) in
                self.mapVC?.monitoringRegion = CLCircularRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: long), radius: radius, identifier: "MonitoringRegion")
                
                // Show Map VC
                self.currentView = .MapView
                self.mapVC?.view.alpha = 0.0
                Utils.addSubviewIntoView(insertedView: (self.mapVC?.view)!, parentView: self.view)
                UIView.animate(withDuration: 0.3, animations: { 
                    self.mapVC?.view.alpha = 1.0
                })
            })
        } else if ((networkStatus == .NetworkStatusCellular) && (currentView != .ListView)) {
            if (currentView == .MapView) {
                // Animation to dismiss view
                self.mapVC?.view.alpha = 1.0
                UIView.animate(withDuration: 0.3, animations: {
                    self.mapVC?.view.alpha = 0.0
                }, completion: { (finished) in
                    self.mapVC?.view.removeFromSuperview()
                })
            }
            // Get new info and push view
            APIManager.getDoctors(latitude: 21.017045, longitude: 105.829707) { (doctors) in
                // Show Tableview VC
                self.currentView = .ListView
                self.doctorsVC?.view.alpha = 0.0
                Utils.addSubviewIntoView(insertedView: (self.doctorsVC?.view)!, parentView: self.view)
                UIView.animate(withDuration: 0.3, animations: {
                    self.doctorsVC?.view.alpha = 1.0
                })
            }
        }
    }
}
