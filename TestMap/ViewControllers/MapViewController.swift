//
//  MapViewController.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import MapKit
import UserNotifications

class MapViewController: UIViewController {
    
    // Outlet
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var ivTrackingCLCheckbox: UIImageView!
    
    // Local variable
    let regionRadius: CLLocationDistance = 500
    let locationManager: CLLocationManager = CLLocationManager()
    var currentLocation:CLLocation?
    var gettingCurrentLocationFirstTime = false
    var disableCenterToCurrentLocation = false
    var isTrackingCurrentLocation: Bool = false
    var monitoringRegion: CLCircularRegion?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupMap()
        self.setupLocationService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Button Actions
    @IBAction func btnTrackingCurrentLocationClicked(_ sender: Any) {
        if (isTrackingCurrentLocation) {
            isTrackingCurrentLocation = false
            ivTrackingCLCheckbox.image = UIImage(named: "unchecked")
        } else {
            isTrackingCurrentLocation = true
            ivTrackingCLCheckbox.image = UIImage(named: "checked")
        }
    }
    
    @IBAction func btnGetCurrentLocation(_ sender: Any) {
        if (currentLocation != nil) {
            self.centerMapOnLocation(location: currentLocation!)
        }
    }
    
    @IBAction func btnGoToRegionClicked(_ sender: Any) {
        if (monitoringRegion != nil) {
            self.centerMapOnLocation(location: CLLocation(latitude: (monitoringRegion?.center.latitude)!, longitude: (monitoringRegion?.center.longitude)!))
        }
    }
    
    
}


// MARK: Map Utils
extension MapViewController: MKMapViewDelegate {
    func setupMap() {
        self.mapView.showsUserLocation = true
        
        let initialLocation = CLLocation(latitude: (monitoringRegion?.center.latitude)!, longitude: (monitoringRegion?.center.longitude)!)
        
        self.centerMapOnLocation(location: initialLocation)
        
        if (self.monitoringRegion != nil) {
            self.addMonitorningRegion(monitoringRegion: self.monitoringRegion!)
            
            self.reverseGeocodeLocation(lat: (self.monitoringRegion?.center.latitude)!, lon: (self.monitoringRegion?.center.longitude)!) { (fullAddress) in
                guard self.currentLocation != nil else {
                    return
                }
                
                self.sendLocalNotification(title: "Locked Target", body: "\(fullAddress)")
            }
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func reverseGeocodeLocation(lat:Double, lon:Double, completionHandler: @escaping (String) -> Swift.Void) {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: lon), completionHandler: {(placemarks, error) -> Void in
            var fullAddress = Constants.kUnknownPlace
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                fullAddress = Constants.kUnknownPlace
            }
            
            if (placemarks?.count)! > 0 {
                let pm = placemarks?[0]
                let lines = pm!.addressDictionary?["FormattedAddressLines"] as? [String]
                if ((lines!.count) > 0) {
                    fullAddress = lines!.joined(separator: ", ")
                }
            }
            else {
                print("Problem with the data received from geocoder")
                fullAddress = Constants.kUnknownPlace
            }
            completionHandler(fullAddress)
        })
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleView = MKCircleRenderer(overlay: overlay)
        circleView.fillColor = UIColor(hex: 0xfb00ff, alpha: 0.3)
        circleView.strokeColor = UIColor.red
        circleView.lineWidth = 2.0
        
        return circleView
    }
    
    func addMonitorningRegion(monitoringRegion:CLCircularRegion) {
        mapView.add(MKCircle(center: monitoringRegion.center, radius: monitoringRegion.radius))
    }
}

// MARK: - CL Location
extension MapViewController: CLLocationManagerDelegate {
    func setupLocationService() {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        gettingCurrentLocationFirstTime = true
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Get location
        let firstLocation:CLLocation = locations[0]
        currentLocation = CLLocation(latitude: firstLocation.coordinate.latitude, longitude: firstLocation.coordinate.longitude)
        
        // Center map to current location
        if (gettingCurrentLocationFirstTime || isTrackingCurrentLocation) {
            gettingCurrentLocationFirstTime = false
            self.centerMapOnLocation(location: currentLocation!)
        }

    }
    
    func getCurrentLocation() {
        if (currentLocation != nil) {
            self.centerMapOnLocation(location: currentLocation!)
        }
    }
    
    func regionMonitoring(sender: AnyObject) {
        let region = CLCircularRegion(center: CLLocationCoordinate2D(latitude: 51.50998, longitude: -0.1337), radius: 1000, identifier: "london")
        region.notifyOnEntry = true
        region.notifyOnExit = true
        locationManager.startMonitoring(for: region)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Did Enter");
        self.sendLocalNotification(title: "Did Enter", body: "You just entered the region")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Did Exit");
        self.sendLocalNotification(title: "Did Exit", body: "You just go out of the region")
    }
    
    @objc(locationManager:didStartMonitoringForRegion:) func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("didStartMonitoringForRegion")
    }
    
    @objc(locationManager:monitoringDidFailForRegion:withError:) func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("monitoringDidFailForRegion: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailWithError:  \(error)")
    }

}

// MARK: Local Notification
extension MapViewController {
    func sendLocalNotification(title: String, body: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5,
                                                        repeats: false)
        let identifier = "LocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
                print("Cannot add Local Notification \(error)")
            }
        })

    }
}
