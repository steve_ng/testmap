//
//  DoctorsViewController.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit

class DoctorsViewController: UIViewController {

    var doctors:[MDoctor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        doctors = CoreDataManager.sharedInstance.getDoctors(offset: 0, limit: Constants.kLimitPerPage)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: TableView delegate & Data source
extension DoctorsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doctors.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.DoctorTableViewCell, for: indexPath) as! DoctorTableViewCell
        let doctor = doctors[indexPath.row]
        cell.updateUIWithModel(mDoctor: doctor, idx: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}
