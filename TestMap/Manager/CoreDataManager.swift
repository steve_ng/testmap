//
//  CoreDataManager.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/24/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import CoreData

class CoreDataManager: NSObject {
    static let sharedInstance : CoreDataManager = {
        let instance = CoreDataManager()
        return instance
    }()
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }

    func storeDotor(name: String, picture: String, age: Int, city: String) {
        let context = getContext()
        
        let entity =  NSEntityDescription.entity(forEntityName: "Doctor", in: context)
        
        let doctor = NSManagedObject(entity: entity!, insertInto: context)
        
        //set the entity values
        doctor.setValue(name, forKey: "name")
        doctor.setValue(picture, forKey: "picture")
        doctor.setValue(age, forKey: "age")
        doctor.setValue(city, forKey: "city")
        
        //save the object
        do {
            try context.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        } catch {
            
        }
    }
    
    func storeDotor(pDoctor: MDoctor) {
        let context = getContext()
        
        let entity =  NSEntityDescription.entity(forEntityName: "Doctor", in: context)
        
        let doctor = NSManagedObject(entity: entity!, insertInto: context)
        
        //set the entity values
        doctor.setValue(pDoctor.name, forKey: Constants.kNameKey)
        doctor.setValue(pDoctor.picture, forKey: Constants.kPictureKey)
        doctor.setValue(pDoctor.age, forKey: Constants.kAgeKey)
        doctor.setValue(pDoctor.city, forKey: Constants.kCityKey)
        
        //save the object
        do {
            try context.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        } catch {
            
        }
    }
    
    func getDoctors (offset: Int, limit: Int) -> [MDoctor] {
        //create a fetch request
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Doctor")
        fetchRequest.fetchLimit = limit
        fetchRequest.fetchOffset = offset
        
        do {
            //go get the results
            let searchResults = try getContext().fetch(fetchRequest)
            
            var doctors:[MDoctor] = []
            for doctorObj in searchResults as [NSManagedObject] {
                let doctor = MDoctor(object: doctorObj)
                doctors.append(doctor)
            }
            return doctors
            
        } catch {
            print("Error with request: \(error)")
            return []
        }
    }
}
