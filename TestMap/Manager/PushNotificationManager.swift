//
//  PushNotificationManager.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import UserNotifications

class PushNotificationManager: NSObject {
    static let sharedInstance : PushNotificationManager = {
        let instance = PushNotificationManager()
        return instance
    }()
    
    func startPushNotification() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
            if (granted) {
                UIApplication.shared.registerForRemoteNotifications()
            } else{
                print("Cannot register for Remote Notification")
            }
        })
    }
}

// MARK: Notification Center Delegate
extension PushNotificationManager: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound])
    }
}
