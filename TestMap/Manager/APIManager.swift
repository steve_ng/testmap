//
//  APIManager.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import Alamofire

class APIManager: NSObject {

    class func getRegion(deviceInfo: String, userInfo: String, completion: ((_ lat: Double, _ long: Double, _ radius: Double) -> Void)?) {
        let parameters: Parameters = [Constants.kUserInfoKey: userInfo,
                                      Constants.kDeviceInfoKey: deviceInfo]
        
        Alamofire.request("http://demo9254566.mockable.io/getregion",
                          method: .post,
                          parameters: parameters).responseJSON { (response) in
                            if let result = response.result.value {
                                let JSON = result as! NSDictionary
                                let lat = JSON.value(forKey: Constants.kLatitudeKey) as! Double
                                let long = JSON.value(forKey: Constants.kLongitudeKey) as! Double
                                let radius = JSON.value(forKey: Constants.kRadiusKey) as! Double
                                if let pCompletion = completion {
                                    pCompletion(lat, long, radius)
                                }
                            }
                            
                            
        }
    }
    
    class func getDoctors(latitude: Double, longitude: Double, completion: ((_ doctors: [MDoctor]) -> Void)?) {
        let parameters: Parameters = [Constants.kLatitudeKey: latitude,
                                      Constants.kLongitudeKey: longitude]
        
        Alamofire.request("http://demo9254566.mockable.io/getdoctors",
                          method: .post,
                          parameters: parameters).responseJSON { (response) in
                            if let result = response.result.value {
                                let JSON = result as! NSArray
                                
                                var doctors:[MDoctor] = []
                                
                                for dict in JSON {
                                    if let doctorDict = dict as? NSDictionary {
                                        let mDoctor = MDoctor(dict: doctorDict)
                                        doctors.append(mDoctor)
                                        CoreDataManager.sharedInstance.storeDotor(pDoctor: mDoctor)
                                    }
                                }
                                if (completion != nil) {
                                    completion!(doctors)
                                }
                            }
        }
    }
}
