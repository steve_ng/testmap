//
//  Constant.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import Foundation

struct Constants {
    // MARK: String
    static let kUserInfoKey             = "user_info"
    static let kDeviceInfoKey           = "device_info"
    static let kLatitudeKey             = "latitude"
    static let kLongitudeKey            = "longitude"
    static let kRadiusKey               = "radius"
    static let kUnknownPlace            = "UnknownPlace"
    static let kFullNameKey             = "full_name"
    static let kNameKey                 = "name"
    static let kCityKey                 = "city"
    static let kAgeKey                  = "age"
    static let kPictureKey              = "picture"
    
    // MARK: Int
    static let kLimitPerPage:Int        = 10
}

struct StoryboardName {
    static let Main                     = "Main"
}

struct ViewControllerName {
    static let MapViewController        = "MapViewController"
    static let DoctorsViewController    = "DoctorsViewController"
}

struct TableViewCellIdentifier {
    static let DoctorTableViewCell      = "DoctorTableViewCell"
}

enum NetworkStatus {
    case NetworkStatusWiFi
    case NetworkStatusCellular
    case NetworkStatusNone
}

enum TopView {
    case MapView
    case ListView
    case RootView
}
