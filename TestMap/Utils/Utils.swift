//
//  Utils.swift
//  TestMap
//
//  Created by Steve Nguyen on 2/23/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit

class Utils: NSObject {
    class func addSubviewIntoView(insertedView: UIView, parentView: UIView) {
        parentView.addSubview(insertedView)
        
        insertedView.translatesAutoresizingMaskIntoConstraints = false
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[insertedView]|", options: [], metrics: nil, views: ["insertedView": insertedView]))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[insertedView]|", options: [], metrics: nil, views: ["insertedView": insertedView]))
        parentView.layoutIfNeeded()
    }
    
    class func showAlertInView(title: String?, message: String?, vc: UIViewController) {
        let alert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
            //Just dismiss the alert
        }
        alert.addAction(cancelAction)
        vc.present(alert, animated: true, completion: nil)
    }
}
