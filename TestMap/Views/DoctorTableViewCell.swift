//
//  DoctorTableViewCell.swift
//  TestMap
//
//  Created by Van Nguyen on 2/24/17.
//  Copyright © 2017 van nguyen. All rights reserved.
//

import UIKit
import SDWebImage

class DoctorTableViewCell: UITableViewCell {

    // Outlet
    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbCity: UILabel!
    @IBOutlet weak var lbAge: UILabel!
    @IBOutlet weak var lbNumber: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateUIWithModel(mDoctor: MDoctor, idx: Int) {
        lbName.text = mDoctor.name
        lbAge.text = "Age:" + String(mDoctor.age)
        lbCity.text = "City:" + mDoctor.city
        lbNumber.text = String(idx + 1)
        
        if let url = URL(string: mDoctor.picture) {
            ivAvatar.sd_setImage(with: url, placeholderImage: UIImage(named: "default_profile"))
        } else {
            ivAvatar.image = UIImage(named: "default_profile")
        }
        
        // Set background
        if (idx%2) == 0 {
            self.backgroundColor = UIColor(hex: 0x2f4152, alpha:0.2)
        } else {
            self.backgroundColor = UIColor(hex: 0x2f4152, alpha:0.4)
        }
    }
}
